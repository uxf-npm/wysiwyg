import { Box } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import React, { FC } from "react";
import { useField } from "react-final-form";
import { ContentRenderer, ToolbarComponent, WysiwygContent, WysiwygEditor } from "../src";
import { RenderComponent } from "../src/Content";
import { ButtonElement, ParagraphComponent } from "./components";

const Toolbar: ToolbarComponent = () => <span>Toolbar not implemented</span>;

export const WysiwygField: FC = () => {
    const { input } = useField<WysiwygContent>("wysiwyg");

    return (
        <>
            <Box mb={2}>
                <WysiwygEditor onChange={input.onChange} value={input.value} Toolbar={Toolbar} plugins={[]} />
            </Box>
            <Box>
                <Card style={{ minHeight: 200 }}>
                    <CardHeader title="Result:" />
                    <CardContent>
                        <ContentRenderer<{ prop: string }, { "solar-lead": RenderComponent }>
                            data={input.value}
                            components={{
                                buttonComponent: ButtonElement,
                                customRenderers: {
                                    "solar-lead": ParagraphComponent,
                                },
                            }}
                            defaultComponentProps={{
                                button: {
                                    ownProps: {
                                        prop: "This is custom prop passed to button component",
                                    },
                                },
                            }}
                        />
                    </CardContent>
                </Card>
            </Box>
        </>
    );
};

WysiwygField.displayName = "WysiwygField";
