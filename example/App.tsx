import React, { useState } from "react";
import { Form } from "react-final-form";
import { WysiwygContent } from "../src";
import { WysiwygField } from "./WysiwygField";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async (values: any) => {
    await sleep(300);
    // eslint-disable-next-line no-alert
    window.alert(JSON.stringify(values));
};

export const App: React.FC = () => {
    const [initialValues, setInitialValues] = useState<{ wysiwyg: WysiwygContent }>({
        wysiwyg: [
            {
                type: "paragraph",
                children: [{ text: "This is initial value.", italic: true, bold: true, underline: true }],
            },
        ],
    });
    return (
        <>
            <Form onSubmit={onSubmit} initialValues={initialValues}>
                {({ handleSubmit }) => {
                    return (
                        <form onSubmit={handleSubmit}>
                            <div>
                                <WysiwygField />
                            </div>
                        </form>
                    );
                }}
            </Form>
            <br />
            <button
                onClick={() =>
                    setInitialValues({
                        wysiwyg: [
                            {
                                type: "paragraph",
                                children: [{ text: Math.random().toString(10) }],
                            },
                        ],
                    })
                }
            >
                Reset initial values
            </button>
        </>
    );
};
