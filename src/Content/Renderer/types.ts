import {
    AnchorHTMLAttributes,
    BlockquoteHTMLAttributes,
    ComponentType,
    DetailedHTMLProps,
    HTMLAttributes,
    ImgHTMLAttributes,
    LiHTMLAttributes,
    OlHTMLAttributes,
    ReactNode,
} from "react";
import {
    EditorButtonElement,
    EditorImageElement,
    EditorLinkElement,
    EditorVideoElement,
    WysiwygContent,
} from "../../types";

export interface RenderComponentProps {
    children: ReactNode;
}

export interface RenderImageComponentProps {
    image: EditorImageElement;
}

export interface RenderLinkComponentProps extends RenderComponentProps {
    link: EditorLinkElement;
}
export interface RenderButtonComponentProps<OwnProps extends Record<string, any> | undefined = undefined> {
    button: EditorButtonElement;
    ownProps?: OwnProps;
}

export interface RenderVideoComponentProps extends Omit<RenderComponentProps, "children"> {
    video: EditorVideoElement;
}

export type VideoWrapperProps = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
    video: EditorVideoElement;
};

export type RenderComponent = ComponentType<RenderComponentProps>;
export type RenderImageComponent = ComponentType<RenderImageComponentProps>;
export type RenderLinkComponent = ComponentType<RenderLinkComponentProps>;
export type RenderVideoComponent = ComponentType<RenderVideoComponentProps>;
export type RenderButtonComponent<OwnProps extends Record<string, any> | undefined = undefined> = ComponentType<
    RenderButtonComponentProps<OwnProps>
>;
export type VideoWrapper = ComponentType<any>;

export interface RenderComponents<
    OwnButtonProps extends Record<string, any> | undefined = undefined,
    CustomRenderers extends Record<string, RenderComponent> | undefined = undefined,
> {
    paragraphComponent?: RenderComponent;
    h1Component?: RenderComponent;
    h2Component?: RenderComponent;
    h3Component?: RenderComponent;
    h4Component?: RenderComponent;
    h5Component?: RenderComponent;
    h6Component?: RenderComponent;
    bulletedListComponent?: RenderComponent;
    numberedListComponent?: RenderComponent;
    listItemComponent?: RenderComponent;
    blockQuoteComponent?: RenderComponent;
    boldComponent?: RenderComponent;
    codeComponent?: RenderComponent;
    underlineComponent?: RenderComponent;
    buttonComponent?: RenderButtonComponent<OwnButtonProps>;
    italicComponent?: RenderComponent;
    imageComponent?: RenderImageComponent;
    linkComponent?: RenderLinkComponent;
    videoWrapper?: VideoWrapper;
    customRenderers?: CustomRenderers;
}

export interface DefaultComponentProps<OwnButtonProps extends Record<string, any> | undefined = undefined> {
    a?: Omit<DetailedHTMLProps<AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>, "href">;
    blockquote?: DetailedHTMLProps<BlockquoteHTMLAttributes<HTMLQuoteElement>, HTMLQuoteElement>;
    code?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
    em?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
    h1?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h2?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h3?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h4?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h5?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h6?: DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    li?: DetailedHTMLProps<LiHTMLAttributes<HTMLLIElement>, HTMLLIElement>;
    ol?: DetailedHTMLProps<OlHTMLAttributes<HTMLOListElement>, HTMLOListElement>;
    p?: DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement>;
    strong?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
    u?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
    ul?: DetailedHTMLProps<HTMLAttributes<HTMLUListElement>, HTMLUListElement>;
    image?: {
        divWrapper?: DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>;
        img?: Omit<DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>, "src">;
        figure?: Omit<DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>, "contentEditable">;
        figcaption?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
        cite?: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
    };
    video?: {
        divWrapper?: VideoWrapperProps;
    };
    button?: DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
        ownProps: OwnButtonProps;
    };
}

export interface ContentRendererProps<
    OwnButtonProps extends Record<string, any> | undefined = undefined,
    CustomRenderers extends Record<string, RenderComponent> | undefined = undefined,
> {
    data?: WysiwygContent;
    components?: RenderComponents<OwnButtonProps, CustomRenderers>;
    defaultComponentProps?: DefaultComponentProps<OwnButtonProps>;
}
