import React, { Fragment, PropsWithChildren } from "react";
import { renderElement } from "./renderElement";
import { renderLeaf } from "./renderLeaf";
import { isLeaf } from "./typeGuards";
import { ContentRendererProps, RenderComponent } from "./types";

export const ContentRenderer = <
    OwnButtonProps extends Record<string, any> | undefined = undefined,
    CustomRenderers extends Record<string, RenderComponent> | undefined = undefined,
>(
    props: PropsWithChildren<ContentRendererProps<OwnButtonProps, CustomRenderers>>,
) => {
    const { data, components, defaultComponentProps } = props;
    if (!data) {
        return null;
    }

    return (
        <>
            {data.map((item, index) => {
                if (isLeaf(item)) {
                    return (
                        <React.Fragment key={index}>
                            {renderLeaf(item, components, defaultComponentProps)}
                        </React.Fragment>
                    );
                }

                return <Fragment key={index}>{renderElement(item, components, defaultComponentProps)}</Fragment>;
            })}
        </>
    );
};
