import React, { ReactNode } from "react";
import { EditorLeafElement } from "../../types";
import { DefaultComponentProps, RenderComponent, RenderComponents } from "./types";

export const renderLeaf = <
    OwnButtonProps extends Record<string, any> | undefined = undefined,
    CustomRenderers extends Record<string, RenderComponent> | undefined = undefined,
>(
    leaf: EditorLeafElement,
    components?: RenderComponents<OwnButtonProps, CustomRenderers>,
    defaultComponentProps?: DefaultComponentProps<OwnButtonProps>,
): ReactNode => {
    let content: React.ReactNode = <>{leaf.text}</>;

    if (leaf.text === "") {
        return <>&#65279;</>;
    }

    if (leaf.bold) {
        const newLeaf = { ...leaf };
        delete newLeaf.bold;

        if (leaf.underline) {
            content = renderLeaf(newLeaf, components, defaultComponentProps);
        }
        if (leaf.italic) {
            content = renderLeaf(newLeaf, components, defaultComponentProps);
        }

        if (components?.boldComponent) {
            return <components.boldComponent>{content}</components.boldComponent>;
        }

        return <strong {...defaultComponentProps?.strong}>{content}</strong>;
    }

    if (leaf.code) {
        if (components?.codeComponent) {
            return <components.codeComponent>{content}</components.codeComponent>;
        }

        return <code {...defaultComponentProps?.code}>{content}</code>;
    }

    if (leaf.italic) {
        const newLeaf = { ...leaf };
        delete newLeaf.italic;

        if (leaf.underline) {
            content = renderLeaf(newLeaf, components, defaultComponentProps);
        }

        if (components?.italicComponent) {
            return <components.italicComponent>{content}</components.italicComponent>;
        }

        return <em {...defaultComponentProps?.em}>{content}</em>;
    }

    if (leaf.underline) {
        if (components?.underlineComponent) {
            return <components.underlineComponent>{content}</components.underlineComponent>;
        }

        return <u {...defaultComponentProps?.u}>{content}</u>;
    }

    return content;
};
