import { EditorLeafElement, SupportedBlockElements } from "../../types";

export function isLeaf(element: SupportedBlockElements | EditorLeafElement): element is EditorLeafElement {
    return element.text !== undefined;
}

export function isLink(element: SupportedBlockElements): boolean {
    return element.type === "link" || !!element.url;
}

export function isImage(element: SupportedBlockElements): boolean {
    return element.type === "image";
}

export function hasUrl(element: SupportedBlockElements): boolean {
    return !!element.url;
}
