import React, { FC } from "react";
import ReactPlayer from "react-player/lazy";
import { RenderVideoComponentProps, VideoWrapper as VideoWrapperComponent, VideoWrapperProps } from "../../types";

const VideoPlayer: FC<RenderVideoComponentProps> = ({ video }) => (
    <ReactPlayer
        style={{
            position: "absolute",
            top: 0,
            left: 0,
            maxWidth: "100%",
            maxHeight: "100%",
        }}
        url={video.url}
        controls={true}
        width="100%"
        height="100%"
    />
);

export const VideoComponent: FC<
    RenderVideoComponentProps & {
        wrapperProps?: VideoWrapperProps;
        VideoWrapper?: VideoWrapperComponent;
    }
> = props => {
    const { video, wrapperProps, VideoWrapper } = props;

    if (VideoWrapper) {
        return (
            <VideoWrapper {...wrapperProps}>
                <VideoPlayer video={video} />
            </VideoWrapper>
        );
    }

    return (
        <div
            style={{
                position: "relative",
                paddingTop: "56.25%" /* 720 / 1280 = 0.5625 */,
            }}
            {...wrapperProps}
        >
            <VideoPlayer video={video} />
        </div>
    );
};
