import { Node } from "slate";
import { WysiwygContent } from "../types";

export const convertToPlainText = (nodes: WysiwygContent, keepIndentation = true) => {
    return nodes.map(n => Node.string(n as Node)).join(keepIndentation ? "\n" : " ");
};
