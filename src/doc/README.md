# WYSIWYG Editor
![alt text](img/example.png "Example")
Editor potřebuje v ts kompilátoru (tsconfig.json) nastavit direktivu
```json
"compilerOptions": {
    "downlevelIteration": true
}
```
Více info zde [https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-3.html](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-3.html)

Obsah na obrázku bude převeden do následující struktury:

```json
[
    { type: "h1", children: [{ text: "H1" }] },
    { type: "h2", children: [{ text: "H2" }] },
    { type: "h3", children: [{ text: "H3" }] },
    { type: "h4", children: [{ text: "H4" }] },
    { type: "paragraph", children: [{ text: "Norm\u00e1ln\u00ed text" }] },
    { type: "paragraph", children: [{ text: "" }] },
    { type: "paragraph", children: [{ text: "Tu\u010dn\u00fd text", bold: true }] },
    { type: "paragraph", children: [{ text: "", bold: true }] },
    { type: "paragraph", children: [{ text: "Text kurz\u00edvou", italic: true }] },
    { type: "paragraph", children: [{ text: "", italic: true }] },
    { type: "paragraph", children: [{ text: "Podtr\u017een\u00fd text", underline: true }] },
    { type: "paragraph", children: [{ text: "", underline: true }] },
    { type: "paragraph", children: [{ text: "Text v elementu code", code: true }] },
    { type: "paragraph", children: [{ text: "", code: true }] },
    {
        type: "paragraph",
        children: [
            {
                text: "Tu\u010dn\u00fd podtr\u017een\u00fd text psan\u00fd kurz\u00edvou",
                bold: true,
                italic: true,
                underline: true,
            },
        ],
    },
    { type: "paragraph", children: [{ text: "", bold: true, italic: true, underline: true }] },
    {
        type: "paragraph",
        children: [
            {
                text:
                    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus ac leo pretium faucibus. Praesent in mauris eu tortor porttitor accumsan. Integer vulputate sem a nibh rutrum consequat. Etiam egestas wisi a erat. Praesent in mauris eu tortor ",
            },
            { text: "porttitor", italic: true },
            { text: " accumsan. Mauris elementum mauris vitae tortor. Aliquam erat volutpat. " },
            { text: "Integer", code: true },
            {
                text:
                    " tempor. Sed ac dolor sit amet purus malesuada congue. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis risus. Aenean id metus id velit ",
            },
            { text: "ullamcorper", bold: true },
            {
                text:
                    " pulvinar.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla pulvinar eleifend sem. Nullam at arcu a est sollicitudin euismod. Integer imperdiet lectus quis justo. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. ",
            },
        ],
    },
    {
        type: "paragraph",
        children: [
            { text: "Praesent in mauris eu tortor porttitor accumsan. Nulla " },
            { text: "accumsan", bold: true },
            {
                text:
                    ", elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Et harum quidem rerum facilis est et expedita distinctio. Nemo enim ipsam voluptatem quia ",
            },
            { text: "voluptas", bold: true },
            {
                text:
                    " sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Vivamus porttitor turpis ac leo. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu ",
            },
            { text: "fugiat", bold: true },
            {
                text:
                    " nulla pariatur. Mauris metus. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In convallis. Donec vitae arcu. Etiam posuere lacus quis dolor. Fusce ",
            },
            { text: "consectetuer", underline: true },
            { text: " risus a nunc. Mauris dictum facilisis augue." },
        ],
    },
]
```
