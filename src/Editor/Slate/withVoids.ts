import { Element as SlateElement } from "slate";
import { UXFEditor } from "../../types";

export const withVoids = (editor: UXFEditor): UXFEditor => {
    const { isVoid } = editor;

    editor.isVoid = (element: SlateElement) => {
        return element.type === "image" ||
            element.type === "video" ||
            element.type === "button" ||
            element.type === "hero-section"
            ? true
            : isVoid(element);
    };

    // editor.insertData = (data: DataTransfer) => {
    //     const text = data.getData("text/plain");
    //     const { files } = data;
    //
    //     if (files && files.length > 0) {
    //         for (const file of files) {
    //             const reader = new FileReader();
    //             const [mime] = file.type.split("/");
    //
    //             if (mime === "image") {
    //                 reader.addEventListener("load", () => {
    //                     const url = reader.result as string;
    //                     insertImage(editor, { type: "image", url, children: [] });
    //                 });
    //
    //                 reader.readAsDataURL(file);
    //             }
    //         }
    //     } else if (isImageUrl(text)) {
    //         insertImage(editor, { type: "image", url: text, children: [] });
    //     } else {
    //         insertData(data);
    //     }
    // };

    return editor;
};
