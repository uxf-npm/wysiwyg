import { UXFEditor } from "../../types";
import {
    createEmptyParagraph,
    getActiveButton,
    getActiveImage,
    getActiveLink,
    getActiveVideo,
    insertButton,
    insertImage,
    insertLink,
    insertVideo,
    isButtonActive,
    isElementActive,
    isImageActive,
    isLinkActive,
    isMarkActive,
    isValidUrl,
    isVideoActive,
    removeEmptyAncestor,
    removeLink,
    toggleElement,
    toggleMark,
    unwrapLink,
    wrapLink,
} from "./utils";

export const withUxfUtils = (editor: UXFEditor): UXFEditor => {
    editor.removeEmptyAncestor = removeEmptyAncestor;
    editor.isMarkActive = isMarkActive;
    editor.toggleMark = toggleMark;
    editor.isElementActive = isElementActive;
    editor.toggleElement = toggleElement;
    editor.isLinkActive = isLinkActive;
    editor.getActiveLink = getActiveLink;
    editor.unwrapLink = unwrapLink;
    editor.wrapLink = wrapLink;
    editor.insertLink = insertLink;
    editor.removeLink = removeLink;
    editor.createEmptyParagraph = createEmptyParagraph;
    editor.insertImage = insertImage;
    editor.insertButton = insertButton;
    editor.isImageActive = isImageActive;
    editor.isButtonActive = isButtonActive;
    editor.getActiveImage = getActiveImage;
    editor.getActiveButton = getActiveButton;
    editor.isVideoActive = isVideoActive;
    editor.getActiveVideo = getActiveVideo;
    editor.insertVideo = insertVideo;
    editor.isValidUrl = isValidUrl;

    return editor;
};
