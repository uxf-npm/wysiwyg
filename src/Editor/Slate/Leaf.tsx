import React from "react";
import { EditorLeafPlugin, EditorLeafRendererProps } from "../../types";

export const Leaf = ({
    attributes,
    children,
    leaf,
    plugins,
    text,
}: EditorLeafRendererProps & { plugins: EditorLeafPlugin[] }) => {
    plugins.map((plugin, index) => {
        children = (
            <plugin.editorLeafRenderer key={`leaf-plugin-${index}`} leaf={leaf} attributes={attributes} text={text}>
                {children}
            </plugin.editorLeafRenderer>
        );
    });

    return <span {...attributes}>{children}</span>;
};
