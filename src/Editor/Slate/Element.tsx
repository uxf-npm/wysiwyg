import React, { ReactElement } from "react";
import { RenderElementProps } from "slate-react";
import { EditorElementPlugins } from "../../types";

export const Element = (
    props: RenderElementProps & {
        plugins: EditorElementPlugins[];
    },
): ReactElement => {
    const { attributes, children, element, plugins } = props;
    const pluginTypes = plugins.map(plugin => plugin.type);

    if (!pluginTypes.find(type => type === element.type)) {
        return <span {...attributes}>{children}</span>;
    }

    return (
        <>
            {plugins.map((plugin, index) => {
                if (plugin.type === element.type) {
                    if ("imageUrlClient" in plugin && "imageNamespace" in plugin) {
                        return (
                            <plugin.editorElementRenderer
                                attributes={attributes}
                                element={element}
                                key={`element-plugin-${index}`}
                                imageNamespace={plugin.imageNamespace}
                                imageUrlClient={plugin.imageUrlClient}
                            >
                                {children}
                            </plugin.editorElementRenderer>
                        );
                    }

                    if ("videoThumbnailImageUrlClient" in plugin && "videoThumbnailImageNamespace" in plugin) {
                        return (
                            <plugin.editorElementRenderer
                                attributes={attributes}
                                element={element}
                                key={`element-plugin-${index}`}
                                videoThumbnailImageNamespace={plugin.videoThumbnailImageNamespace}
                                videoThumbnailImageUrlClient={plugin.videoThumbnailImageUrlClient}
                            >
                                {children}
                            </plugin.editorElementRenderer>
                        );
                    }

                    return (
                        <plugin.editorElementRenderer
                            key={`element-plugin-${index}`}
                            attributes={attributes}
                            element={element}
                        >
                            {children}
                        </plugin.editorElementRenderer>
                    );
                }

                return null;
            })}
        </>
    );
};
