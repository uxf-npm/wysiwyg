import { createEditor } from "slate";
import { withHistory } from "slate-history";
import { withReact } from "slate-react";
import { withVoids } from "./withVoids";
import { withLinks } from "./withLinks";
import { withUxfUtils } from "./withUxfUtils";

export const createUxfEditor = () => withUxfUtils(withVoids(withLinks(withHistory(withReact(createEditor())))));
