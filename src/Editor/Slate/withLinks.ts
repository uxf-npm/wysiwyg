import { Element as SlateElement, Node, Transforms } from "slate";
import { UXFEditor } from "../../types";

export const withLinks = (editor: UXFEditor): UXFEditor => {
    const { isInline, insertData, normalizeNode } = editor;

    editor.isInline = (element: SlateElement): boolean => {
        return element.type === "link" ? true : isInline(element);
    };

    editor.normalizeNode = entry => {
        const [node, path] = entry;
        // If the element is a paragraph, ensure its children are valid.
        if (SlateElement.isElement(node) && node.type === "paragraph") {
            for (const [child, childPath] of Node.children(editor, path)) {
                if (SlateElement.isElement(child) && !editor.isInline(child)) {
                    Transforms.unwrapNodes(editor, { at: childPath });
                    return;
                }
            }
        }

        // Fall back to the original `normalizeNode` to enforce other constraints. http://uxf.cz
        normalizeNode(entry);
    };

    editor.insertData = (data: DataTransfer) => {
        const text = data.getData("text/plain");
        if (editor.isValidUrl(text)) {
            editor.insertLink(editor, text, text);
        } else {
            insertData(data);
        }
    };

    return editor;
};
