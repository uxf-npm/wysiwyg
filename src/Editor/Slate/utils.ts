import isUrl from "is-url";
import { Editor, Element as SlateElement, Range, Transforms } from "slate";
import { jsx } from "slate-hyperscript";
import {
    EditorBlockElement,
    EditorButtonElement,
    EditorImageElement,
    EditorLeafElement,
    EditorLinkElement,
    EditorVideoElement,
    LeafMarks,
    SupportedBlockElementTypes,
    UXFEditor,
} from "../../types";

const LIST_TYPES = ["numbered-list", "bulleted-list"];

export const removeEmptyAncestor = (editor: UXFEditor, range?: Range | null) => {
    const activeNode = range ? (editor.children[range.anchor.path[0]] as EditorBlockElement | EditorLeafElement) : null;
    if (
        activeNode &&
        activeNode.type === "paragraph" &&
        (activeNode.children as Array<{ text?: string }>)[0]?.text === ""
    ) {
        Transforms.removeNodes(editor, {
            at: range ? range : undefined,
        });
    }
};

export const isMarkActive = (editor: UXFEditor, mark: LeafMarks): boolean => {
    const marks = Editor.marks(editor);
    return marks ? marks[mark] === true : false;
};

export const toggleMark = (editor: UXFEditor, format: LeafMarks): void => {
    const isActive = isMarkActive(editor, format);

    if (isActive) {
        Editor.removeMark(editor, format);
    } else {
        Editor.addMark(editor, format, true);
    }
};

export const isElementActive = (editor: UXFEditor, format: SupportedBlockElementTypes): boolean => {
    const [match] = Editor.nodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === format,
    });

    return !!match;
};

export const toggleElement = (editor: UXFEditor, format: SupportedBlockElementTypes): void => {
    const isActive = isElementActive(editor, format);
    const isList = LIST_TYPES.includes(format);

    Transforms.unwrapNodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && LIST_TYPES.includes(n.type),
        split: true,
    });

    Transforms.setNodes(editor, {
        type: isActive ? "paragraph" : isList ? "list-item" : format,
    });

    if (!isActive && isList) {
        const block = { type: format, children: [] };
        Transforms.wrapNodes(editor, block);
    }
};

export const isLinkActive = (editor: UXFEditor): boolean => {
    const [link] = Editor.nodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "link",
    });
    return !!link;
};

export const getActiveLink = (editor: UXFEditor): EditorLinkElement | null => {
    if (isLinkActive(editor)) {
        const [link] = Editor.nodes(editor, {
            match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "link",
        });
        return link[0] as EditorLinkElement;
    }

    return null;
};

export const unwrapLink = (editor: UXFEditor): void => {
    Transforms.unwrapNodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "link",
    });
};

export const wrapLink = (editor: Editor, url: string, text?: string, target?: "_blank" | "_self"): void => {
    if (isLinkActive(editor)) {
        unwrapLink(editor);
    }

    const { selection } = editor;
    const isCollapsed = selection && Range.isCollapsed(selection);
    const link: EditorLinkElement = {
        type: "link",
        url,
        target,
        children: isCollapsed ? [{ text: text ? text : url }] : [],
    };

    if (isCollapsed) {
        Transforms.insertNodes(editor, link);
    } else {
        Transforms.wrapNodes(editor, link, { split: true });
        Transforms.collapse(editor, { edge: "end" });
    }
};

export const insertLink = (editor: UXFEditor, url: string, text?: string, target?: "_blank" | "_self"): void => {
    if (editor.selection) {
        wrapLink(editor, url, text, target);
    }
};

export const removeLink = (editor: UXFEditor): void => {
    if (editor.selection) {
        unwrapLink(editor);
    }
};

export const createEmptyParagraph = () => jsx("element", { type: "paragraph" }, [jsx("text")]);

export const insertImage = (editor: UXFEditor, image: EditorImageElement, range?: Range | null): void => {
    Transforms.insertNodes(editor, [jsx("element", { ...image }, [jsx("text")]), createEmptyParagraph()], {
        at: range ? range : undefined,
    });
    removeEmptyAncestor(editor, range);
};

export const insertButton = (editor: UXFEditor, buttonText: string, buttonUrl: string, range?: Range | null): void => {
    Transforms.insertNodes(
        editor,
        [jsx("element", { type: "button", buttonUrl, buttonText }, [jsx("text")]), createEmptyParagraph()],
        {
            at: range ? range : undefined,
        },
    );
    removeEmptyAncestor(editor, range);
};

export const isImageActive = (editor: UXFEditor): boolean => {
    const [button] = Editor.nodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "image",
    });
    return !!button;
};

export const isButtonActive = (editor: UXFEditor): boolean => {
    const [image] = Editor.nodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "button",
    });
    return !!image;
};

export const getActiveImage = (editor: UXFEditor): EditorImageElement | null => {
    if (isImageActive(editor)) {
        const [image] = Editor.nodes(editor, {
            match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "image",
        }) as any;
        return image[0] as EditorImageElement;
    }

    return null;
};

export const getActiveButton = (editor: UXFEditor): EditorButtonElement | null => {
    if (isButtonActive(editor)) {
        const [button] = Editor.nodes(editor, {
            match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "button",
        }) as any;
        return button[0] as EditorButtonElement;
    }

    return null;
};

export const isVideoActive = (editor: UXFEditor): boolean => {
    const [video] = Editor.nodes(editor, {
        match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "video",
    });
    return !!video;
};

export const getActiveVideo = (editor: UXFEditor): EditorVideoElement | null => {
    if (isImageActive(editor)) {
        const [video] = Editor.nodes(editor, {
            match: n => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "video",
        }) as any;
        return video[0] as EditorVideoElement;
    }

    return null;
};

export const insertVideo = (editor: UXFEditor, video: EditorVideoElement, range?: Range | null): void => {
    Transforms.insertNodes(editor, [jsx("element", { ...video }, [jsx("text")]), createEmptyParagraph()], {
        at: range ? range : undefined,
    });
    removeEmptyAncestor(editor, range);
};

export const isValidUrl = (url?: string) => !!url && isUrl(url);
