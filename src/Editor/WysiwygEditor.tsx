import { isKeyHotkey } from "is-hotkey";
import React, { FC, useCallback, useMemo } from "react";
import { Descendant, Range, Transforms } from "slate";
import { Editable, RenderElementProps, Slate } from "slate-react";
import { EditorElementPlugins, EditorLeafPlugin, EditorLeafRendererProps, WysiwygEditorProps } from "../types";
import { createUxfEditor, Element, Leaf } from "./Slate";

export const WysiwygEditor: FC<WysiwygEditorProps> = ({
    value,
    onChange,
    placeholder = "Napište něco hezkého...",
    plugins,
    Toolbar,
    classNameForEditable,
    stylesForEditable,
}) => {
    const editor = useMemo(createUxfEditor, []);
    const initialValue: Descendant[] = [
        {
            type: "paragraph",
            children: [{ text: "" }],
        },
    ];

    const elementPlugins: EditorElementPlugins[] = useMemo(() => {
        const el: EditorElementPlugins[] = [];
        plugins.forEach(p => {
            if ("editorElementRenderer" in p) {
                el.push(p);
            }
        });
        return el;
    }, [plugins]);

    const leafPlugins: EditorLeafPlugin[] = useMemo(() => {
        const el: EditorLeafPlugin[] = [];
        plugins.forEach(p => {
            if ("editorLeafRenderer" in p) {
                el.push(p);
            }
        });
        return el;
    }, [plugins]);

    const renderElement = useCallback(
        (props: RenderElementProps) => <Element {...props} plugins={elementPlugins} />,
        [elementPlugins],
    );

    const renderLeaf = useCallback(
        (props: EditorLeafRendererProps) => <Leaf {...props} plugins={leafPlugins} />,
        [leafPlugins],
    );

    /**
        Default left/right behavior is unit:'character'.
        This fails to distinguish between two cursor positions, such as
        <inline>foo<cursor/></inline> vs <inline>foo</inline><cursor/>.
        Here we modify the behavior to unit:'offset'.
        This lets the user step into and out of the inline without stepping over characters.
        You may wish to customize this further to only use unit:'offset' in specific cases.
    */
    const onKeyDown: React.KeyboardEventHandler<HTMLInputElement> = event => {
        const { selection } = editor;

        if (selection && Range.isCollapsed(selection)) {
            const { nativeEvent } = event;
            if (isKeyHotkey("left", nativeEvent)) {
                event.preventDefault();
                Transforms.move(editor, { unit: "offset", reverse: true });
                return;
            }

            // disable inline mode on right key press
            if (isKeyHotkey("right", nativeEvent)) {
                event.preventDefault();
                Transforms.move(editor, { unit: "offset" });
            }

            // disable insert link mode on space key press and move caret
            if (isKeyHotkey("space", nativeEvent) && editor.isLinkActive(editor)) {
                event.preventDefault();
                Transforms.move(editor, { unit: "offset" });
                editor.insertText(" ");
            }
        }
    };

    /*
     * value is initial value only
     * @see https://github.com/ianstormtaylor/slate/issues/4765
     * @see https://github.com/ianstormtaylor/slate/pull/4540#issuecomment-951903419
     * @see https://github.com/ianstormtaylor/slate/pull/4768/files
     * */

    editor.children = value ? value : initialValue;

    return (
        <Slate editor={editor} value={initialValue} onChange={onChange}>
            <Toolbar plugins={plugins} />
            <Editable
                renderElement={renderElement}
                spellCheck={false}
                renderLeaf={renderLeaf}
                placeholder={placeholder}
                className={classNameForEditable}
                style={stylesForEditable}
                onKeyDown={onKeyDown}
            />
        </Slate>
    );
};
