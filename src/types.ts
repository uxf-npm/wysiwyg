import { CSSProperties, FC } from "react";
import { BaseEditor, Editor, Range } from "slate";
import { HistoryEditor } from "slate-history";
import {
    ReactEditor,
    RenderElementProps as SlateRenderElementProps,
    RenderLeafProps as SlateRenderLeafProps,
} from "slate-react";

type DateString = `${number}-${number}-${number}`;

export interface WysiwygEditorProps {
    value?: EditorBlockElement[];
    onChange: (event: any) => void;
    placeholder?: string;
    plugins: EditorPlugin[];
    Toolbar: ToolbarComponent;
    classNameForEditable?: string;
    stylesForEditable?: CSSProperties;
}

export type ImageUrlClient = (uuid: string, extension: string, namespace?: string) => string; // storageClient TODO dependency
export type VideoThumbnailImageUrlClient = ImageUrlClient;
export type ImageUploadHandler = (
    file: File,
    namespace: string,
) => Promise<{
    id: number;
    uuid: string;
    type: string;
    extension: string;
    name: string;
    namespace?: string | null;
    url?: string;
}>;
export type VideoThumbnailImageUploadHandler = ImageUploadHandler;

export type FontStyle = "italic" | "bold" | "underline" | "code";
export type LeafMarks = FontStyle;

export type Headings = "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "paragraph";
export type Lists = "bulleted-list" | "list-item" | "numbered-list";
export type SupportedBlockElementTypes<SUPPORTED_TYPES = string> =
    | Headings
    | Lists
    | "block-quote"
    | "link"
    | "image"
    | "video"
    | "button"
    | SUPPORTED_TYPES;

export type EditorBlockElementChildren = Array<SupportedBlockElements | EditorLeafElement>;

export interface EditorBlockElement<SUPPORTED_ELEMENTS = SupportedBlockElementTypes> {
    type: SUPPORTED_ELEMENTS;
    children: EditorBlockElementChildren;
    [key: string]: any;
}

type PickedLinkProps = Pick<
    import("react").DetailedHTMLProps<import("react").AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>,
    "target" | "rel" | "download" | "hrefLang" | "media" | "referrerPolicy"
>;

interface OwnLinkProps extends EditorBlockElement {
    type: "link";
    url: string;
}

export type EditorLinkElement = PickedLinkProps & OwnLinkProps;

export interface EditorImageElement extends EditorBlockElement {
    type: "image";
    uuid?: string;
    extension?: string;
    name?: string;
    namespace?: string;
    source?: string;
    caption?: string;
    alt?: string;
    url?: string;
}

export interface EditorVideoElement extends EditorBlockElement {
    name?: string;
    type: "video";
    uploadedAt: DateString | undefined;
    url?: string;
    thumbnail?: {
        uuid?: string;
        extension?: string;
        name?: string;
        namespace?: string;
        url?: string;
    };
}

export interface EditorButtonElement extends EditorBlockElement {
    type: "button";
    buttonUrl: string;
    buttonText: string;
}

export type SupportedBlockElements<AdditionalElements = EditorBlockElement> =
    | EditorBlockElement
    | EditorImageElement
    | EditorLinkElement
    | EditorVideoElement
    | EditorButtonElement
    | AdditionalElements;

/**
 * Marks are either present and true or undefined
 */
export interface EditorLeafElement {
    text: string;
    bold?: true;
    italic?: true;
    underline?: true;
    code?: true;
    [key: string]: unknown;
}

export type WysiwygContent = Array<SupportedBlockElements>;

export interface UXFEditor extends BaseEditor, ReactEditor, HistoryEditor {
    removeEmptyAncestor: (editor: UXFEditor, range?: Range | null) => void;
    isMarkActive: (editor: UXFEditor, mark: LeafMarks) => boolean;
    toggleMark: (editor: UXFEditor, format: LeafMarks) => void;
    isElementActive: (editor: UXFEditor, format: SupportedBlockElementTypes) => boolean;
    toggleElement: (editor: UXFEditor, format: SupportedBlockElementTypes) => void;
    isLinkActive: (editor: UXFEditor) => boolean;
    getActiveLink: (editor: UXFEditor) => EditorLinkElement | null;
    unwrapLink: (editor: UXFEditor) => void;
    wrapLink: (editor: Editor, url: string, text?: string, target?: "_blank" | "_self") => void;
    insertLink: (editor: UXFEditor, url: string, text?: string, target?: "_blank" | "_self") => void;
    removeLink: (editor: UXFEditor) => void;
    createEmptyParagraph: () => SupportedBlockElements<EditorBlockElement<string>>;
    insertButton: (editor: UXFEditor, buttonText: string, buttonUrl: string, range?: Range | null) => void;
    insertImage: (editor: UXFEditor, image: EditorImageElement, range?: Range | null) => void;
    isImageActive: (editor: UXFEditor) => boolean;
    isButtonActive: (editor: UXFEditor) => boolean;
    getActiveImage: (editor: UXFEditor) => EditorImageElement | null;
    getActiveButton: (editor: UXFEditor) => EditorButtonElement | null;
    isVideoActive: (editor: UXFEditor) => boolean;
    getActiveVideo: (editor: UXFEditor) => EditorVideoElement | null;
    insertVideo: (editor: UXFEditor, video: EditorVideoElement, range?: Range | null) => void;
    isValidUrl: (url?: string) => boolean;
}

declare module "slate" {
    interface CustomTypes {
        Editor: UXFEditor;
        Element: SupportedBlockElements;
        Text: EditorLeafElement;
    }
}

export interface EditorElementPlugin {
    toolbarButton?: FC<ToolbarButtonProps>;
    editorElementRenderer: FC<EditorElementRendererProps>;
    type: string;
}

export type EditorElementPlugins = EditorElementPlugin | EditorImageElementPlugin | EditorVideoElementPlugin;

export interface EditorLeafPlugin {
    toolbarButton: FC<ToolbarButtonProps>;
    editorLeafRenderer: FC<EditorLeafRendererProps>;
    type: string;
}

export interface ToolbarButtonDivider {
    buttonDivider: FC;
}

export type EditorPlugin =
    | EditorElementPlugin
    | EditorLeafPlugin
    | ToolbarButtonDivider
    | EditorImageElementPlugin
    | EditorVideoElementPlugin;

export interface ToolbarButtonProps {
    editor: UXFEditor;
}

export type EditorElementRendererProps = SlateRenderElementProps;

export interface EditorLeafRendererProps extends SlateRenderLeafProps {
    leaf: EditorLeafElement;
}

export interface EditorImageElementPlugin {
    toolbarButton: FC<ToolbarImageButtonProps>;
    editorElementRenderer: FC<EditorImageElementRendererProps>;
    type: string;
    imageUploadHandler: ImageUploadHandler;
    imageUrlClient: ImageUrlClient;
    imageNamespace: string;
}

export interface EditorVideoElementPlugin {
    isOnlyVideoUrlRequired?: boolean;
    toolbarButton: FC<ToolbarVideoButtonProps>;
    editorElementRenderer: FC<EditorVideoElementRendererProps>;
    type: string;
    videoThumbnailImageUploadHandler: VideoThumbnailImageUploadHandler;
    videoThumbnailImageUrlClient: VideoThumbnailImageUrlClient;
    videoThumbnailImageNamespace: string;
}

export interface ToolbarImageButtonProps extends ToolbarButtonProps {
    imageUploadHandler: ImageUploadHandler;
    imageNamespace: string;
}

export interface ToolbarVideoButtonProps extends ToolbarButtonProps {
    isOnlyVideoUrlRequired?: boolean;
    videoThumbnailImageUploadHandler: VideoThumbnailImageUploadHandler;
    videoThumbnailImageNamespace: string;
}

export interface EditorImageElementRendererProps extends EditorElementRendererProps {
    imageUrlClient: ImageUrlClient;
    imageNamespace: string;
}

export interface EditorVideoElementRendererProps extends EditorElementRendererProps {
    videoThumbnailImageUrlClient: VideoThumbnailImageUrlClient;
    videoThumbnailImageNamespace: string;
}

export interface ToolbarProps {
    plugins: EditorPlugin[];
}

export type ToolbarComponent = FC<ToolbarProps>;
