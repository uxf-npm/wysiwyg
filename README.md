# DEPRECATED UXF Wysiwyg editor
[![npm](https://img.shields.io/npm/v/@uxf/wysiwyg)](https://www.npmjs.com/package/@uxf/wysiwyg)
[![size](https://img.shields.io/bundlephobia/min/@uxf/wysiwyg)](https://www.npmjs.com/package/@uxf/wysiwyg)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/wysiwyg)](https://www.npmjs.com/package/@uxf/wysiwyg)
[![license](https://img.shields.io/npm/l/@uxf/wysiwyg)](https://www.npmjs.com/package/@uxf/wysiwyg)

Use Node < 17

Package depends on @uxf/storage (PHP BE)

#Installation

### Web
```bash
yarn add @uxf/wysiywg-editor
``` 

### CMS
```bash
yarn add @uxf/wysiywg-editor
# Install peer dependencies if not installed
yarn add @material-ui/core @material-ui/icons @material-ui/lab
``` 
